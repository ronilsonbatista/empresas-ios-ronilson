//
//  Enterprise.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

struct Enterprise: Codable {
    
    let enterprises: [Enterprises]?
    
    enum CodingKeys: String, CodingKey {
        case enterprises
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.enterprises = try values.decode([Enterprises]?.self, forKey: .enterprises)
    }
}
