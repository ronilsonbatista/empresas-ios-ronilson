//
//  StartSearchViewCell.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit

class StartSearchViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.textColor = .colorGrayPrimary
    }
}
