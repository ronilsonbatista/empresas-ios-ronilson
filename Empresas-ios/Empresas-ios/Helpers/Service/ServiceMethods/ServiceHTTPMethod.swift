//
//  ServiceHTTPMethod.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 02/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

enum ServiceHTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case trace = "TRACE"
    case connect = "CONNECT"
}
