//
//  ServiceManager.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 02/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation
import Alamofire

final class ServiceManager: ServiceManagerProtocol {
    
    func request(method: ServiceHTTPMethod, url: String, parameters: [String : Any]?, encoding: ServiceEncoding, success: @escaping (Data) -> Void, failure: @escaping ((ServiceError) -> ())) {
    
        let requestHeader: [String: String] = {
            guard let accessToken = RequestHeader.shared.accessToken,
                let client = RequestHeader.shared.client, let uid = RequestHeader.shared.uid else {
                    return ["": ""]
            }
            let header = ["access-token": accessToken,
                          "client": client,
                          "uid": uid]
            return header
        }()
        
        // Type used to define how a set of parameters are applied to request
        let requestEncoding: ParameterEncoding = {
            switch encoding {
            case .default: return URLEncoding.default
            case .json: return JSONEncoding.default
            }
        }()
        
        // HTTP method used
        let requestMethod = HTTPMethod(rawValue: method.rawValue)!
        
        if !Connectivity.isConnectedToInternet() {
            failure(ServiceError(type: .noConnection))
            return
        }
        
        // Request
        Alamofire.request(url, method: requestMethod, parameters: parameters, encoding: requestEncoding, headers: requestHeader)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    guard let responseData = response.data else {
                        failure(ServiceError(type: .badRequest))
                        return
                    }
                    
                    self.savedHeader(response)
                    success(responseData)
                    
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        failure(ServiceError(type: .timeout))
                        return
                    }
                    
                    switch response.response?.statusCode {
                    case 403: failure(ServiceError(type: .forbidden))
                    case 401: failure(ServiceError(type: .unauthorized))
                    case 400: break;
                    default: failure(self.handleError(with: response))
                    }
                }
        }
    }
    
    private func savedHeader(_ response: DataResponse<Any>) {
        let accessToken = self.requestHeader(response: response, header: "access-token")
        RequestHeader.shared.accessToken = accessToken
        
        let uid = self.requestHeader(response: response, header: "uid")
       RequestHeader.shared.uid = uid
        
        let client = self.requestHeader(response: response, header: "client")
        RequestHeader.shared.client = client
        
    }
    private func requestHeader(response: DataResponse<Any>, header: String) -> String {
        if let header = response.response?.allHeaderFields[header] as? String {
            return header
        }
        return ""
    }
    
    private func handleError(with response: DataResponse<Any>) -> ServiceError {
        
        guard let statusCode = response.response?.statusCode else {
            return ServiceError(type: .badRequest)
        }
        
        switch statusCode {
        case ServiceError.ErrorType.badRequest.code:
            return ServiceError(type: .badRequest, object: response.data)
        default:
            let errorType = ServiceError.ErrorType(statusCode: statusCode)
            return ServiceError(type: errorType)
        }
    }
}
