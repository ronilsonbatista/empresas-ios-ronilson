//
//  SearchEnterprisesPresenter.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class SearchEnterprisesPresenter {
    
    fileprivate unowned let view: SearchEnterprisesProtocol
    fileprivate let service: SearchEnterprisesService
    fileprivate(set) var enterprisesViewModel: [EnterprisesViewModel] = []
    
    init(view: SearchEnterprisesProtocol) {
        self.view = view
        self.service = SearchEnterprisesService()
    }
}

// MARK: - Public methods

extension SearchEnterprisesPresenter {
    
    func setupInitialization() {
        self.view.navigationBarConfiguration()
    }
    
    func removeAll() {
        self.enterprisesViewModel.removeAll()
    }
    
    func searchEnterprises(with text: String) {
        self.view.startLoading()
        if text.isEmpty {
            self.view.reloadTableView()
            return
        }
        self.service.getEnterprises(name: text, success: { enterprises in
            guard let enterprises = enterprises else  {
                return
            }
            self.handleEnterprisesData(at: enterprises.enterprises ?? [])
            self.view.reloadTableView()
            self.view.stopLoading()
        }) { error in
            self.view.showAlertError(with: "Erro encontrado", message: error.type.description, buttonTitle: "OK")
            self.view.stopLoading()
        }
    }
}

// MARK: - Private methods

extension SearchEnterprisesPresenter {
    
    fileprivate func handleEnterprisesData(at enterprises: [Enterprises]) {
        for enterprise in enterprises {
            self.enterprisesViewModel.append(
                EnterprisesViewModel(enterpriseName: enterprise.enterpriseName, country: enterprise.country ?? "", enterpriseTypeName: enterprise.enterpriseType?.enterpriseTypeName ?? "", photo: "http://empresas.ioasys.com.br/\(enterprise.photo ?? " " )", descriptionEnterprises: enterprise.description ?? "")
            )
        }
    }
}
