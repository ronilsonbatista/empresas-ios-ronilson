//
//  SearchEnterprisesService.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class SearchEnterprisesService {
    
    func getEnterprises(name: String, success: @escaping (Enterprise?) -> Void, fail: @escaping (_ error: ServiceError) -> Void) {
        
        let serviceUrl = "\(Url.domain.value)\(Url.version.value)\(Url.enterprises.value)"
        
        let parameters = ["name": name ] as [String: Any]
        
        ServiceRequest.shared.request(method: .get, url: serviceUrl, parameters: parameters, encoding: .default, success: { result in
            do {
                let enterprises = try JSONDecoder().decode(Enterprise.self, from: result)
                success(enterprises)
            } catch {
                success(nil)
            }
        }, failure: { serviceError  in
            fail(serviceError)
        })
    }
}
