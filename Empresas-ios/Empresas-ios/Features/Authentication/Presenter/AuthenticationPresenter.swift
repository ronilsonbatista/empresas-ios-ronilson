//
//  AuthenticationPresenter.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class AuthenticationPresenter {
    
    fileprivate unowned let view: AuthenticationProtocol
    fileprivate let service: AuthenticationService
    
    init(view: AuthenticationProtocol) {
        self.view = view
        self.service = AuthenticationService()
    }
}

// MARK: - Public methods

extension AuthenticationPresenter {
    
    func authentication(with email: String, with password: String) {
        self.view.startLoading()
        
        let continueAuthentication = self.checkFields(at: email, at: password)
        
        if continueAuthentication {
            self.view.reloadTableView()
            self.view.stopLoading()
            return
        }
        
        self.service.authentication(email: email, password: password, success: { success in
            print(success)
            self.view.stopLoading()
            self.view.goNextScreen()
        }) { erorr in
            if erorr.type == ServiceError.ErrorType.unauthorized {
                self.view.showAlertError(with: "Erro encontrado", message: "Credenciais de login inválidas. Por favor, tente novamente.", buttonTitle: "OK")
            } else {
                self.view.showAlertError(with: "Erro encontrado", message: erorr.type.description, buttonTitle: "OK")
            }
            self.view.stopLoading()
        }
    }
}

// MARK: - Private methods

extension AuthenticationPresenter {
    
    fileprivate func checkFields(at email: String, at password: String) -> Bool {
        
        if email.isEmpty && password.isEmpty {
            self.view.handleError(isEmailError: true, isPasswordEroor: true)
            return true
        }
        if email.isEmpty {
            self.view.handleError(isEmailError: true, isPasswordEroor: false)
            return true
        }
        if password.isEmpty {
            self.view.handleError(isEmailError: false, isPasswordEroor: true)
            return true
        }
        
        self.view.handleError(isEmailError: false, isPasswordEroor: false)
        return false
    }
}
