//
//  ServiceURL.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 04/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

enum Url {
    case domain
    case version
    case authentication
    case enterprises
    
    var value: String {
        switch self {
        case .domain: return "http://empresas.ioasys.com.br/"
        case .version: return "api/v1/"
        case .authentication: return "users/auth/sign_in"
        case .enterprises: return "enterprises"
        }
    }
}
