//
//  EnterprisesViewModel.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class EnterprisesViewModel: NSObject {
    
    var enterpriseName: String?
    var country: String?
    var enterpriseTypeName: String?
    var photo: String?
    var descriptionEnterprises: String?
    
    init(enterpriseName: String?, country: String, enterpriseTypeName: String, photo: String, descriptionEnterprises: String) {
        
        self.enterpriseName = enterpriseName
        self.country = country
        self.enterpriseTypeName = enterpriseTypeName
        self.photo = photo
        self.descriptionEnterprises = descriptionEnterprises
    }
}
