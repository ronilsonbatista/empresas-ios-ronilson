//
//  DetailsEnterpriseViewController.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit

class DetailsEnterpriseViewController: UITableViewController {
    
    var enterprise: EnterprisesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBarConfiguration()
        self.tableView.register(UINib(nibName: DetailsEnterpriseViewCell.identifier, bundle: nil), forCellReuseIdentifier: DetailsEnterpriseViewCell.identifier)
    }
}

// MARK: - Table view data source

extension DetailsEnterpriseViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 574
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let enterprise = self.enterprise else { return UITableViewCell() }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:DetailsEnterpriseViewCell.identifier, for: indexPath) as? DetailsEnterpriseViewCell {
            cell.fillOutlets(enterprises: enterprise)
            return cell
        }
        return UITableViewCell()
    }
    
}

// MARK: - Layout Methods

extension DetailsEnterpriseViewController {
    
    func navigationBarConfiguration() {
        self.title = enterprise?.enterpriseName
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15.0, weight: UIFont.Weight.semibold)]
        self.navigationController?.navigationBar.barTintColor = .colorDarkishPink
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.tintColor = .white
    }
}
