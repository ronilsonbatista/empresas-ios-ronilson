//
//  AuthenticationService.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class AuthenticationService {
        
    func authentication(email: String, password: String, success: @escaping (Data) -> Void, fail: @escaping (_ error: ServiceError) -> Void) {
        
        let serviceUrl = "\(Url.domain.value)\(Url.version.value)\(Url.authentication.value)"
        
        let parameters = ["email": email,
                          "password": password ] as [String: Any]
        
        ServiceRequest.shared.request(method: .post, url: serviceUrl, parameters: parameters, encoding: .json, success: { result in
            success(result)
        }, failure: { serviceError  in
            fail(serviceError)
        })
    }
}
