//
//  SearchEnterprisesViewController.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchEnterprisesViewController: UITableViewController {
    
    fileprivate var presenter: SearchEnterprisesPresenter!
    fileprivate var searchController: UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = .colorBackground
        self.tableView.register(UINib(nibName: StartSearchViewCell.identifier, bundle: nil), forCellReuseIdentifier: StartSearchViewCell.identifier)
        self.tableView.register(UINib(nibName: ResultSearchViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultSearchViewCell.identifier)
        
        self.presenter = SearchEnterprisesPresenter(view: self)
        self.presenter.setupInitialization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

// MARK: - UITableViewDataSource

extension SearchEnterprisesViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return 1 }
        return self.presenter.enterprisesViewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let count = self.presenter.enterprisesViewModel.count
        
        if indexPath.section == 0 && indexPath.row == 0 && count == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier:StartSearchViewCell.identifier, for: indexPath) as? StartSearchViewCell {
                return cell
            }
        }
        
        if count > 0 {
            let enterprises = self.presenter.enterprisesViewModel
            
            var isHiddenView = Bool()
            if enterprises.last?.enterpriseName == enterprises[indexPath.row].enterpriseName { isHiddenView = true }
            else { isHiddenView = false }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier:ResultSearchViewCell.identifier, for: indexPath) as? ResultSearchViewCell {
                cell.fillOutlets(enterprises: enterprises[indexPath.row], isHidden: isHiddenView)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let nextController = DetailsEnterpriseViewController()
        nextController.enterprise = self.presenter.enterprisesViewModel[indexPath.row]
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let count = self.presenter.enterprisesViewModel.count
        if indexPath.section == 0 && indexPath.row == 0 {
            return count > 0  ? 0: 137
        }
        return 113
    }
}

// MARK: - UISearchBarDelegate

extension SearchEnterprisesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.removeAll()
        self.presenter.searchEnterprises(with: searchText)
        self.tableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.reloadTableView()
        print("cancel")
    }
}

// MARK: - SearchEnterprisesProtocol

extension SearchEnterprisesViewController: SearchEnterprisesProtocol {
    func startLoading() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setForegroundColor(.colorGreenyBlue)
        SVProgressHUD.setBackgroundColor(.colorBackground)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func stopLoading() {
        SVProgressHUD.dismiss()
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func showAlertError(with title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func navigationBarConfiguration() {
        let logoImage: UIImage = UIImage(named: "logoNav")!
        self.navigationItem.titleView = UIImageView(image: logoImage)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15.0, weight: UIFont.Weight.semibold)]
        self.navigationController?.navigationBar.barTintColor = .colorDarkishPink
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icSearch"), style: .plain, target: self, action: #selector(searchButtonTouched))
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.setHidesBackButton(true, animated :true)
    }
    
    @objc func searchButtonTouched() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.tintColor = .lightGray
        self.searchController.searchBar.barTintColor = .colorDarkishPink
        self.presenter.removeAll()
        self.reloadTableView()
        present(searchController, animated: true, completion: nil)
    }
}
