//
//  DetailsEnterpriseViewCell.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit

class DetailsEnterpriseViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var descriptionLabel: UILabel!
    @IBOutlet weak fileprivate var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.textColor = .colorGrayPrimary
    }
    
    func fillOutlets(enterprises: EnterprisesViewModel) {
        self.descriptionLabel.text = enterprises.descriptionEnterprises
    
        if let imageString = enterprises.photo, let imageURL = URL(string: imageString) {
            self.photoImageView!.af_setImage(withURL: imageURL)
        }
    }
}
