//
//  Enterprises.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

struct Enterprises: Codable {
    
    let id: Int?
    let enterpriseName: String?
    let photo: String?
    let description: String?
    let country: String?
    let enterpriseType: EnterpriseType?
    
    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseName = "enterprise_name"
        case photo
        case description
        case country
        case enterpriseType = "enterprise_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decode(Int?.self, forKey: .id)
        self.enterpriseName = try values.decode(String?.self, forKey: .enterpriseName)
        self.photo = try values.decode(String?.self, forKey: .photo)
        self.description = try values.decode(String?.self, forKey: .description)
        self.country = try values.decode(String?.self, forKey: .country)
        self.enterpriseType = try values.decode(EnterpriseType?.self, forKey: .enterpriseType)
    }
}
