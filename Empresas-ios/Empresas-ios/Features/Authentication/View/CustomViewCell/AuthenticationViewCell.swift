//
//  AuthenticationViewCell.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit

protocol AuthenticationViewCellDelegate: class {
    func emailText(at text: String)
    func passwordText(at text: String)
}

class AuthenticationViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet weak fileprivate var subTitleLabel: UILabel!
    @IBOutlet weak fileprivate var passwordErrorLabel: UILabel!
    @IBOutlet weak fileprivate var emailErrorLabel: UILabel!
    @IBOutlet weak fileprivate var emailTextField: UITextField!
    @IBOutlet weak fileprivate var passwordTextField: UITextField!
    @IBOutlet weak fileprivate var separatorLineOneView: UIView!
    @IBOutlet weak fileprivate var separatorLineTwoView: UIView!
    
    var delegate: AuthenticationViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.textColor = .colorGrayPrimary
        self.subTitleLabel.textColor = .colorGrayPrimary
        self.separatorLineOneView.backgroundColor = .colorCharcoalGrey
        self.separatorLineTwoView.backgroundColor = .colorCharcoalGrey
        
        self.emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        self.emailWithoutError()
        self.passwordWithoutError()
    }
    
    func handleError(at isEmailError: Bool, at isPasswordEroor: Bool) {
        if isEmailError && isPasswordEroor {
            self.emailWithError()
            self.passwordWithError()
            return
        }
        if isEmailError {
            self.emailWithError()
            return
        }
        if isPasswordEroor {
            self.passwordWithError()
            return
        }
    }
}
// MARK: - Handle Error

extension AuthenticationViewCell {
    
    func emailWithError() {
        self.emailErrorLabel.isHidden = false
        self.separatorLineOneView.backgroundColor = .red
    }
    
    func passwordWithError() {
        self.passwordErrorLabel.isHidden = false
        self.separatorLineTwoView.backgroundColor = .red
    }
    
    func emailWithoutError() {
        self.emailErrorLabel.isHidden = true
        self.separatorLineOneView.backgroundColor = .colorCharcoalGrey
    }
    
    func passwordWithoutError() {
        self.passwordErrorLabel.isHidden = true
        self.separatorLineTwoView.backgroundColor = .colorCharcoalGrey
    }
}

// MARK: - UITextFieldDelegate

extension AuthenticationViewCell: UITextFieldDelegate {
    
    @objc fileprivate func textFieldDidChange(_ textField: UITextField) {
        if textField == self.emailTextField {
            self.emailWithoutError()
            self.delegate?.emailText(at: textField.text ?? "")
            return
        }
        
        if textField == self.passwordTextField {
            self.passwordWithoutError()
            self.delegate?.passwordText(at: textField.text ?? "")
            return
        }
    }
}
