//
//  AuthenticationViewController.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit
import SVProgressHUD

class AuthenticationViewController: UITableViewController {
    
    fileprivate var presenter: AuthenticationPresenter!
    
    fileprivate var emailText: String? = ""
    fileprivate var passwordText: String? = ""

    fileprivate var isEmailError: Bool = false
    fileprivate var isPasswordEroor: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = AuthenticationPresenter(view: self)
        
        self.tableView.register(UINib(nibName: AuthenticationViewCell.identifier, bundle: nil), forCellReuseIdentifier: AuthenticationViewCell.identifier)
        self.tableView.backgroundColor = .colorBackground
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

// MARK: - UITableViewDataSource

extension AuthenticationViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let customFooter = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 53))
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 324, height: 53))
        button.center = customFooter.center
        button.backgroundColor = .colorGreenyBlue
        button.layer.cornerRadius = 6.0
        button.setTitle("ENTRAR", for: .normal)
        button.titleLabel!.font = UIFont.systemFont(ofSize: 20.0, weight: UIFont.Weight.semibold)
        button.addTarget(self, action: #selector(self.authentication), for: .touchUpInside)
        customFooter.addSubview(button)
        
        return customFooter
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 53
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:AuthenticationViewCell.identifier, for: indexPath) as? AuthenticationViewCell {
            cell.delegate = self
            cell.handleError(at: self.isEmailError, at: self.isPasswordEroor)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
}

// MARK: - AuthenticationViewCellDelegate

extension AuthenticationViewController: AuthenticationViewCellDelegate {
    
    func emailText(at text: String) {
        self.emailText = text
    }
    
    func passwordText(at text: String) {
        self.passwordText = text
    }
}

// MARK: - Action methods

extension AuthenticationViewController {
    
    @objc fileprivate func authentication() {
        self.presenter.authentication(with: self.emailText ?? "", with: self.passwordText ?? "")
    }
}

// MARK: - AuthenticationProtocol

extension AuthenticationViewController: AuthenticationProtocol {

    func startLoading() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setForegroundColor(.colorGreenyBlue)
        SVProgressHUD.setBackgroundColor(.colorBackground)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func stopLoading() {
        SVProgressHUD.dismiss()
    }
    
    func goNextScreen() {
        let nextController = SearchEnterprisesViewController()
        self.navigationController?.pushViewController(nextController, animated: false)
    }
    
    func handleError(isEmailError: Bool, isPasswordEroor: Bool) {
        self.isEmailError = isEmailError
        self.isPasswordEroor = isPasswordEroor
    }
    
    func showAlertError(with title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func reloadTableView() {
        UIView.transition(with: tableView!, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.tableView?.reloadData()
        })
    }
}
