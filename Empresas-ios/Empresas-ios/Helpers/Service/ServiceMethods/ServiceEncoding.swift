//
//  ServiceEncoding.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 02/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

enum ServiceEncoding {
    case `default`
    case json
}
