//
//  EnterpriseType.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

struct EnterpriseType: Codable {
    
    let id: Int?
    let enterpriseTypeName: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decode(Int?.self, forKey: .id)
        self.enterpriseTypeName = try values.decode(String?.self, forKey: .enterpriseTypeName)
    }
}
