//
//  AppControl.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

class RequestHeader {
    
    static public let shared = RequestHeader()
    
    var accessToken: String?
    var client: String?
    var uid: String?
}
