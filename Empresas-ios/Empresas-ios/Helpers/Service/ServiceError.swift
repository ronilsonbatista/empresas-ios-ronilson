//
//  ServiceError.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 02/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

struct ServiceError {
    var type: ErrorType
    var object: Any?
    
    init(type: ErrorType, object: Any? = nil) {
        self.type = type
        self.object = object
    }
    
    enum ErrorType {
        case tokenNotFound
        case missinParam
        case networkError
        case badRequest
        case unauthorized
        case forbidden
        case notFound
        case internalServerError
        case serviceUnavailable
        case httpUntreated
        case timeout
        case noConnection
        case cancelledRequest
        
        var description: String {
            switch self {
            case .tokenNotFound: return "Token não encontrado"
            case .missinParam: return "Parâmetro ausente ou incorreto em resposta"
            case .networkError: return "Erro de rede"
            case .badRequest: return "Bad Request"
            case .unauthorized: return "Não autorizado"
            case .forbidden: return "Forbidden"
            case .notFound: return "Não encontrado"
            case .internalServerError: return "Erro do Servidor Interno"
            case .serviceUnavailable: return "Serviço indisponível"
            case .httpUntreated: return "Erro de HTTP não tratado"
            case .timeout: return "Conexão esgotada"
            case .noConnection: return "Conexão indisponível"
            case .cancelledRequest: return "Solicitação foi cancelada"
            }
        }
        
        var code: Int {
            switch self {
            case .badRequest: return 400
            case .unauthorized: return 401
            case .forbidden: return 403
            case .notFound: return 404
            case .internalServerError: return 500
            case .serviceUnavailable: return 503
            default: return -1
            }
        }
        
        init(statusCode: Int) {
            switch statusCode {
            case 400: self = .badRequest
            case 401: self = .unauthorized
            case 403: self = .forbidden
            case 404: self = .notFound
            case 500: self = .internalServerError
            case 503: self = .serviceUnavailable
            default: self = .httpUntreated
            }
        }
    }
}
