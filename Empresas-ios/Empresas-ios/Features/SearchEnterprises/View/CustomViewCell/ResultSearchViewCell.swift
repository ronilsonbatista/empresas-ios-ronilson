//
//  ResultSearchViewCell.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import UIKit
import AlamofireImage

class ResultSearchViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var enterpriseNameLabel: UILabel!
    @IBOutlet weak fileprivate var countryLabel: UILabel!
    @IBOutlet weak fileprivate var enterpriseTypeNameLabel: UILabel!
    @IBOutlet weak fileprivate var photoImageView: UIImageView!
    @IBOutlet weak fileprivate var separatorLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.enterpriseNameLabel.textColor = .colorGrayPrimary
        self.countryLabel.textColor = .colorWarmGrey
        self.enterpriseTypeNameLabel.textColor = .colorWarmGrey
        self.separatorLineView.backgroundColor = .colorBackground
    }
    
    func fillOutlets(enterprises: EnterprisesViewModel, isHidden: Bool) {
        self.separatorLineView.isHidden = isHidden
        self.enterpriseNameLabel.text = enterprises.enterpriseName
        self.countryLabel.text = enterprises.country
        self.enterpriseTypeNameLabel.text = enterprises.enterpriseTypeName
        
        if let imageString = enterprises.photo, let imageURL = URL(string: imageString) {
            self.photoImageView!.af_setImage(withURL: imageURL)
        }
    }
}
