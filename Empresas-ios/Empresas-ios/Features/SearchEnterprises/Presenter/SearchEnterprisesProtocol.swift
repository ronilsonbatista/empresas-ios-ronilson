//
//  SearchEnterprisesProtocol.swift
//  Empresas-ios
//
//  Created by Ronilson Batista on 03/02/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

protocol SearchEnterprisesProtocol: class {
    
    func startLoading()
    func stopLoading()
    func reloadTableView()
    func navigationBarConfiguration()
    func searchButtonTouched()
    func showAlertError(with title: String, message: String, buttonTitle: String)
}
